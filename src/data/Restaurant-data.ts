import {RestaurantDto} from './RestaurantDto';

export const RESTAURANTS: RestaurantDto[] = [{
    name: 'Arrosto', dishes: [{
        name: 'knedlo zelo vepro', price: '123', dish_id: 24
    }, {
       name: 'kachna na mliku', price: '124', dish_id: 25
    }]
}, {
    name: 'Cas', dishes: [{
        name: 'tucnak v ledu', price: '125', dish_id: 23
    }, {
        name: 'Otziho stehno', price: '126', dish_id: 22
    }]
}, {
    name: 'Smrtak', dishes: [{
        name: 'maso s omackou - trocha tucneho masa zalita omackou', price: '125', dish_id: 21
    }, {
        name: 'vegetariangske ragu - supr jidlo pro vegetariany', price: '149', dish_id: 20
    }, {
        name: 'palacinky na sladko - marmelada jahodova nebo malinova + slehacka', price: '106', dish_id: 19
    }]
}, {
    name: 'Palatino', dishes: [{
        name: 'testoviny rajcatove - trocha testovin s hodne rajcaty', price: '125', dish_id: 18
    }, {
        name: 'pizza Hawai - pripecena pizza: sunka, anannas, smetanovy zaklad', price: '149', dish_id: 17
    }, {
        name: 'pizza zampionova - lehce nedopečená pizza s hafou žampionů', price: '106', dish_id: 16
    }]
}, {
    name: 'Red Hook', dishes: [{
        name: 'burrito', price: '129', dish_id: 15
    }, {
        name: 'burger bez napadu', price: '149', dish_id: 14
    }, {
        name: 'gnoči - smetanova omacka a v ni testovinove knedlicky', price: '132', dish_id: 13
    }]
}, {
    name: 'Dalsi restaurace 1', dishes: [{
        name: 'tucnak v ledu', price: '125', dish_id: 12
    }, {
        name: 'Otziho stehno', price: '126', dish_id: 11
    }]
}, {
    name: 'Dalsi restaurace 2', dishes: [{
        name: 'maso s omackou - trocha tucneho masa zalita omackou', price: '125', dish_id: 10
    }, {
        name: 'vegetariangske ragu - supr jidlo pro vegetariany', price: '149', dish_id: 9
    }, {
       name: 'palacinky na sladko - marmelada jahodova nebo malinova + slehacka', price: '106', dish_id: 8
    }]
}, {
    name: 'Dalsi restaurace 3', dishes: [{
        name: 'testoviny rajcatove - trocha testovin s hodne rajcaty', price: '125', dish_id: 7
    }, {
        name: 'pizza Hawai - pripecena pizza: sunka, anannas, smetanovy zaklad', price: '149', dish_id: 6
    }, {
       name: 'pizza zampionova - lehce nedopečená pizza s hafou žampionů', price: '106', dish_id: 5
    }]
}];
