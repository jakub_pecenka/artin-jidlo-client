import {Dish} from '@src/data/Dish';

export class RestaurantDto {
    name: string;
    dishes: Dish[];

    constructor() {
        this.name = undefined;
        this.dishes = undefined;
    }

}

