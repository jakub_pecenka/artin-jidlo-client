import { Routes } from '@angular/router';

import { HomeComponent } from '@src/app/home/home.component';
import {FoodDetailComponent} from '@src/app/food-detail/food-detail.component';

export const routes: Routes = [
  {
      path: '',
      redirectTo: '/home',
      pathMatch: 'full',
  },
  {
      path: 'home',
      component: HomeComponent,
  },
    {
        path: 'description',
        component: FoodDetailComponent,
    },
];
