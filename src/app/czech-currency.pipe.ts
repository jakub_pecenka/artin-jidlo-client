import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'czechCurrency'})
export class CzechCurrency implements PipeTransform {
    transform(value: number): string {
        if (!value) {
            return ' Kč';
        }
        return value.toString() + ' Kč';
    }
}
