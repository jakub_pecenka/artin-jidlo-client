import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {plainToClass} from 'class-transformer';
import {RestaurantDto} from '@src/data/RestaurantDto';
import {Dish} from '@src/data/Dish';

@Injectable()
export class AppService {

    private _actualDish: Dish;

    constructor(private http: HttpClient) {
    }

    getRestaurants(): Observable<RestaurantDto[]> {
        return this.http.get(`http://localhost:8080//kandelabr`)
            .pipe(map(restaurants => (restaurants as any[])
                .map(restaurantDto => plainToClass(RestaurantDto, restaurantDto))));
    }

    get actualDish(): Dish {
        return this._actualDish;
    }

    set actualDish(value: Dish) {
        this._actualDish = value;
    }
}
