import {Component, Input, OnInit} from '@angular/core';
import {RestaurantDto} from '@src/data/RestaurantDto';
import {ActivatedRoute, Router} from '@angular/router';
import {AppService} from '@src/app/app.service';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css'],
})
export class RestaurantComponent implements OnInit {

  @Input() restaurant: RestaurantDto;

  constructor(private router: Router, private route: ActivatedRoute, private appService: AppService) { }

  ngOnInit() {
  }

  showDescription(menu) {

    this.appService.actualDish = menu;
    this.router.navigate(['./description']);

  }

}
