import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';

import { AppRoutingModule } from '@src/app/app-routing.module';
import { AppComponent } from '@src/app/app.component';
import { HomeComponent } from '@src/app/home/home.component';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import {RestaurantComponent} from '@src/app/restaurant/restaurant.component';
import {FoodDetailComponent} from '@src/app/food-detail/food-detail.component';
import {AppService} from '@src/app/app.service';
import {CzechCurrency} from '@src/app/czech-currency.pipe';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';

// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from 'nativescript-angular/forms';

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';

@NgModule({
  declarations: [AppComponent, HomeComponent, RestaurantComponent, FoodDetailComponent, CzechCurrency], imports: [NativeScriptModule, // NATIVESCRIPT
    AppRoutingModule, HttpClientModule, NativeScriptFormsModule, CommonModule],
  providers: [AppService],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA] // disable angular html templates checks
})
export class AppModule { }
