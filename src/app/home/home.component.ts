import {Component, OnInit} from '@angular/core';
import {AppService} from '@src/app/app.service';
import {RestaurantDto} from '@src/data/RestaurantDto';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {async} from 'rxjs/Scheduler/async';
import {BehaviorSubject} from 'rxjs';

@Component({
    selector: 'app-home', templateUrl: './home.component.html', styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
    title = 'Aktuální menu v blízkosti Artin-Praha';
    allRestaurants: RestaurantDto[];

    public items$: BehaviorSubject<Array<any>> = new BehaviorSubject([]);
     public allRestaurantsObservable: Observable<RestaurantDto[]> = new BehaviorSubject([]);


    constructor(private appService: AppService) {
        this.allRestaurants = [];


        let items: any[] = [
            {title: 'NativeScript'},
            {title: 'Angular'},
            {title: 'TypeScript'},
            {title: 'JavaScript'}
        ];
        let cnt = 0;

        let timer = setInterval(() => {
            if (cnt < 4) {
                this.items$.next([...this.items$.getValue(), items[cnt]]);
                cnt++;
            } else {
                clearInterval(timer);
            }
        }, 1000);
    }

    ngOnInit() {

        this.allRestaurantsObservable = this.appService.getRestaurants()

        this.appService.getRestaurants()
            .toPromise()
            .then(restaurants => {
                this.allRestaurants = restaurants;
            });
        const x = new Observable(observer => {
            observer.next('hi');
            observer.complete();
        });
    }

}
