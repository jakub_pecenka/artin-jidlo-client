import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from '@src/app/app-routing.module';
import { AppComponent } from '@src/app/app.component';
import { HomeComponent } from '@src/app/home/home.component';
import {RestaurantComponent} from '@src/app/restaurant/restaurant.component';
import {FoodDetailComponent} from '@src/app/food-detail/food-detail.component';
import {AppService} from '@src/app/app.service';
import {CzechCurrency} from '@src/app/czech-currency.pipe';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RestaurantComponent,
    FoodDetailComponent,
      CzechCurrency
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
