import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppService} from '@src/app/app.service';
import {Dish} from '@src/data/Dish';

@Component({
  selector: 'app-food-detail',
  templateUrl: './food-detail.component.html',
  styleUrls: ['./food-detail.component.css'],
})
export class FoodDetailComponent implements OnInit {

  dish: Dish;

  constructor(private route: ActivatedRoute, private router: Router, private appService: AppService) {
  }

  ngOnInit() {
    this.dish = this.appService.actualDish;

  }

  onNavBtnTap() {
    // This code will be called only in Android.
    this.router.navigate(['home']);

    console.log('Navigation button tapped!');
  }

}
